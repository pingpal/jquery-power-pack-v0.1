-function (name, wrapper) { 'use strict';
    var f = function () {
        var that = this, args = arguments, f = function () {
            return wrapper.apply(that, args)
        }

        var instance = f() || {}
        instance.factory = f
        return instance
    }

    if (typeof exports !== 'undefined') {
        module.exports = f(require('jquery'), require('spin.js'))

    } else if (typeof define === 'function' && define.amd) {
        define(['jquery', 'spin.js'], f)

    } else {
        window[name] = f(window.jQuery, window.Spinner)
    }

}('btn-work-indctr', function($, Spinner) { "use strict";
	var side_kick = 'btn-work-indctr'
	var sideKick = side_kick.toLowerCase().replace(/-(.)/g,function(m,g){return g.toUpperCase()})

	$[sideKick] = function(el, options) {
		this.options = options
		this.$el = $(el)
		this.init()
	}

	$[sideKick].DEFAULTS = {
		delay: 250
	}

	$[sideKick].prototype.init = function () {

		this.options.spinner = this.options.spinner || {}

		var spinner = {
			  lines: 7 // The number of lines to draw
			, length: 4 // The length of each line
			, width: 3 // The line thickness
			, radius: 6 // The radius of the inner circle
			, scale: 1 // Scales overall size of the spinner
			, corners: 1 // Corner roundness (0..1)
			, color: '#000' // #rgb or #rrggbb or array of colors
			, opacity: 0.25 // Opacity of the lines
			, rotate: 0 // The rotation offset
			, direction: 1 // 1: clockwise, -1: counterclockwise
			, speed: 2 // Rounds per second
			, trail: 60 // Afterglow percentage
			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
			, zIndex: 2e9 // The z-index (defaults to 2000000000)
			, className: 'spinner' // The CSS class to assign to the spinner
			, top: '50%' // Top position relative to parent
			, left: '50%' // Left position relative to parent
			, shadow: false // Whether to render a shadow
			, hwaccel: false // Whether to use hardware acceleration
			, position: 'absolute' // Element positioning
		}

		this.options.spinner = $.extend({}, spinner, this.options.spinner)

		this.$el.css('position', 'relative')
	}

	$[sideKick].prototype.resume = function(callback) {
		var that = this

		if (!this.working) {

			this.working = true

			setTimeout(function () {

				if (that.working) {

					that.color = that.$el.css('color')
					that.$el.css('color', 'rgb(0, 0, 0, 0)')

					that.options.addClass && that.$el.addClass(that.options.addClass)
					that.options.delClass && that.$el.removeClass(that.options.delClass)

					that.$el.attr('disabled', 'disabled')

					new Spinner(that.options.spinner).spin(that.$el[0])
				}

			}, this.options.delay)
		}
	}

	$[sideKick].prototype.resign = function(callback) {

		if (this.working) {

			this.working = false

			this.$el.removeAttr('disabled')

			this.options.addClass && this.$el.addClass(this.options.delClass)
			this.options.delClass && this.$el.removeClass(this.options.addClass)

			this.$el.css('color', this.color)

			this.$el.find('.spinner').remove()
		}
	}

	$.fn[sideKick] = function(x) {
		var v, y = typeof x == 'object' && x
		var a = Array.prototype.slice.call(arguments, 1)

		this.each(function() {
			var t = $(this), d = t.data(sideKick)
			var o = $.extend({}, $[sideKick].DEFAULTS, t.data(), y)
			if (!d) t.data(sideKick, (d = new $[sideKick](this, o)))
			if (typeof x == 'string') v = $[sideKick].prototype[x].apply(d, a)
		})

		return v !== undefined ? v : this
	}

	$.fn[sideKick].Constructor = $[sideKick]
});
