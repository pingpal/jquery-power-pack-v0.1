-function (name, wrapper) { 'use strict';
    var f = function () {
        var that = this, args = arguments, f = function () {
            return wrapper.apply(that, args)
        }

        var instance = f() || {}
        instance.factory = f
        return instance
    }

    if (typeof exports !== 'undefined') {
        module.exports = f(require('jquery'))

    } else if (typeof define === 'function' && define.amd) {
        define(['jquery'], f)

    } else {
        window[name] = f(window.jQuery)
    }

}('notify', function($) { "use strict";
	var notify, Notify = function () {}

	Notify.prototype.setHtml = function (html) {
		notify = html
	}

	Notify.prototype.open = function(string, success) {
		var wrap, text, complete = function () {}

		if (notify) {
			if (typeof success === 'undefined') {
				wrap = '.modal-alert-wrap'
				text = '.modal-alert-text'

			} else {
				wrap = success ? '.modal-alert-wrap-success' : '.modal-alert-wrap-failure'
				text = success ? '.modal-alert-text-success' : '.modal-alert-text-failure'
			}

			$('input:focus').blur()
			$('.modal').modal('hide')
			var modal = $(notify).modal()

			$(document).on('keydown.notify', function (e) {
				(e.which == 13 || e.which == 27) && modal.modal('hide')
			})

			modal.on('hide.bs.modal', function () {
				$(document).off('keydown.notify')
				complete() | modal.remove()
			})

			modal.find(wrap).show().find(text).html(string || '')

		} else {
			alert(string)
		}

		return {
			onComplete : function (callback) {
				notify ? complete = callback : callback()
			}
		}
	}

	$.notify = new Notify()
});
