-function (name, wrapper) { 'use strict';
    var f = function () {
        var that = this, args = arguments, f = function () {
            return wrapper.apply(that, args)
        }

        var instance = f() || {}
        instance.factory = f
        return instance
    }

    if (typeof exports !== 'undefined') {
        module.exports = f(require('jquery'))

    } else if (typeof define === 'function' && define.amd) {
        define(['jquery'], f)

    } else {
        window[name] = f(window.jQuery)
    }

}('shake', function($) {
	$.fn.shake = function(shakes, distance, duration) {
        shakes = shakes || 4
        distance = distance || 5
        duration = duration || 600

		this.each(function() {
			$(this).css('position', 'relative')
			for (var x = 1; x <= shakes; x++) {
				$(this).animate({ left: distance * -1 }, duration/shakes/4)
					.animate({ left: distance }, duration/shakes/2)
					.animate({ left: 0 }, duration/shakes/4)
			}
		})

		return this
	}

    $.fn.cSSShake = function () {
        var ends = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'

        this.each(function() {
            $(this).addClass('cssshake').one(ends, function () {
                $(this).removeClass('cssshake')
                this.offsetWidth = this.offsetWidth
            })
        })

        return this
    }
});
