-function (name, wrapper) { 'use strict';
    var f = function () {
        var that = this, args = arguments, f = function () {
            return wrapper.apply(that, args)
        }

        var instance = f() || {}
        instance.factory = f
        return instance
    }

    if (typeof exports !== 'undefined') {
        module.exports = f(require('jquery'))

    } else if (typeof define === 'function' && define.amd) {
        define(['jquery'], f)

    } else {
        window[name] = f(window.jQuery)
    }

}('verify', function($) { "use strict";
	var verify, Verify = function () {}

	Verify.prototype.setHtml = function (html) {
		verify = html
	}

	Verify.prototype.open = function(e, string) {
		e = e || undefined

		var $el = e && $(e.target), failure, success = failure = function () {}
		var $cl = e && $el.closest('[data-confirm], [data-confirm-text], [data-confirm-link]')

		$cl && $cl.length && ($el = $cl)

		var result = {
			confirmed: e && !!e.confirmed,

			onSuccess: function (callback) {
				if (verify) {
					success = callback

				} else if(result.confirmed) {
					callback()
				}

				return result
			},

			onFailure: function (callback) {
				if (verify) {
					failure = callback

				} else if(!result.confirmed) {
					callback()
				}

				return result
			}
		}

		var prevent = function () {
			e && e.preventDefault() | e.stopImmediatePropagation()
		}

		var trigger = function () {
			result.confirmed = true

			if (e) {
				var e2 = jQuery.Event(e.type)
				e2.confirmed = true
				e2.which = e.which
				$el.trigger(e2)
			}
		}

		var getConfirmText = function () {
			return string || e && $el.data('confirm-text') || ''
		}

		if (!result.confirmed) {
			if (verify) {
				prevent()

				$('input:focus').blur()
				$('.modal').modal('hide')
				var modal = $(verify).modal()

				$(document).on('keydown.verify', function (e2) {
					e2.which == 13 && trigger() | modal.modal('hide')
					e2.which == 27 && modal.modal('hide')
				})

				modal.on('hide.bs.modal', function () {
					$(document).off('keydown.verify')
					result.confirmed ? success() : failure()
					modal.remove()
				})

				modal.find('.modal-confirm-text').html(getConfirmText())
				modal.find('.modal-confirm-ok').click(function() {
					trigger()
				})

			} else {
				if (confirm(getConfirmText())) {
					result.confirmed = true

				} else {
					prevent()
				}
			}
		}

		result.confirmed && e && $el.is('[data-confirm-link]') && e.preventDefault() | (location.href = $el.attr('href'))

		return result
	}

	$.verify = new Verify()

	$(document).on('click', '[data-confirm], [data-confirm-text], [data-confirm-link]', $.verify.open)
});
